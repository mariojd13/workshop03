<?php
  include('functions.php');

  if(isset($_POST['full_name']) && isset($_POST['email']) ) {
    $saved = saveStudent($_POST);

    if($saved) {
      header('Location: /workshop03/?status=success');
    } else {
      header('Location: /workshop03/?status=error');
    }
  } else {
    header('Location: /workshop03/?status=error');
  }
